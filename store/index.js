import React, { createContext } from "react";
import CommonStore from "./common";
import UserStore from "./user";
import VideoStore from "./video";

const StoreContext = createContext({});

const StoreProvider = ({ children }) => {
  return (
    <StoreContext.Provider value={{}}>
      <CommonStore>
        <UserStore>
          <VideoStore>{children}</VideoStore>
        </UserStore>
      </CommonStore>
    </StoreContext.Provider>
  );
};

export default StoreProvider;
