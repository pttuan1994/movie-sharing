import { useReducer, createContext, useContext, useEffect } from "react";
import {
  getAuth,
  EmailAuthProvider,
  linkWithCredential,
  sendEmailVerification,
  signInAnonymously,
  signInWithCustomToken,
  signInWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
} from "firebase/auth";
import { getFunctions, httpsCallable } from "firebase/functions";
import { getDatabase, ref, get, update, remove } from "firebase/database";
import { produce } from "immer";

import useLocalStorage from "@/utils/useLocalStorage";
import { ACCESS_TOKEN, ERROR_CODE } from "@/constants/store";
import { GET_ACCESS_TOKEN } from "@/constants/function";
import { useCommon } from "./common";

const initialState = {
  profile: null,
};

const reducer = produce((draft, action) => {
  switch (action.type) {
    case "setProfile":
      draft.profile = action.payload;
      break;
    default:
      break;
  }
});

const UserContext = createContext({});

const UserStore = ({ children }) => {
  const [token, setToken] = useLocalStorage(ACCESS_TOKEN, {});
  const [state, dispatch] = useReducer(reducer, initialState);
  const { firebase } = useCommon();

  const loadUser = async (user) => {
    const database = getDatabase();
    try {
      if (user && user.email) {
        const userData = await get(ref(database, `users/${user.uid}`));
        if (userData.exists()) {
          dispatch({
            type: "setProfile",
            payload: { uid: user.uid, ...userData.val() },
          });
        } else {
          dispatch({
            type: "setProfile",
            payload: null,
          });
        }
      } else
        dispatch({
          type: "setProfile",
          payload: null,
        });
    } catch (error) {
      // Most probably a connection error. Handle appropriately.
    }
  };

  const signInByAnonymous = () =>
    new Promise((resolve, reject) => {
      const auth = getAuth();
      const functions = getFunctions();
      const database = getDatabase();
      signInAnonymously(auth)
        .then((userCredential) => {
          const { uid, metadata } = userCredential.user;
          const urlParams = new URLSearchParams(window.location.search);
          const referCode = urlParams.get("ref");

          const userData = {
            referredBy: referCode,
            metadata: metadata,
          };
          update(ref(database, "users/" + uid), userData);
          const getAccessToken = httpsCallable(functions, GET_ACCESS_TOKEN);
          getAccessToken({ uid })
            .then(({ data }) => {
              console.log("signInAnonymously", data);
              setToken({ value: data });
              resolve(data);
            })
            .catch((error) => {
              console.log("Error creating custom token:", error);
              reject(error);
            });
        })
        .catch((error) => {
          const errorCode = error.code;
          reject(errorCode);
        });
    });

  const signInByEmailPassword = ({ email, password }) =>
    new Promise((resolve, reject) => {
      const auth = getAuth();
      const functions = getFunctions();
      const database = getDatabase();
      const currentUser = auth.currentUser;
      if (!currentUser.email) {
        remove(ref(database, "users/" + currentUser.uid));
      }
      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          const user = userCredential.user;
          const getAccessToken = httpsCallable(functions, GET_ACCESS_TOKEN);
          getAccessToken({ uid: user.uid })
            .then(({ data }) => {
              console.log("signInByEmailPassword", data);
              loadUser(user).then(() => {
                setToken({ value: data });
                resolve(data);
              });
            })
            .catch((error) => {
              console.log("Error creating custom token:", error);
              reject(error);
            });
        })
        .catch((error) => {
          const errorCode = error.code;
          reject(errorCode);
        });
    });

  const signInUser = ({ email, password }) =>
    new Promise((resolve, reject) => {
      const auth = getAuth();
      const database = getDatabase();
      // Convert an anonymous account to a permanent account
      const credential = EmailAuthProvider.credential(email, password);
      linkWithCredential(auth.currentUser, credential)
        .then((usercred) => {
          const user = usercred.user;
          console.log("Anonymous account successfully upgraded", user);
          const userData = {
            email: user.email,
            metadata: user.metadata,
          };
          update(ref(database, "users/" + user.uid), userData);
          signInByEmailPassword({ email, password })
            .then((data) => {
              resolve(data);
            })
            .catch((error) => {
              reject(error);
            });
        })
        .catch((error) => {
          console.log("Error upgrading anonymous account", error);
          const errorCode = error.code;
          if (
            errorCode === ERROR_CODE.PROVIDER_ALREADY_LINKED ||
            errorCode === ERROR_CODE.EMAIL_ALREADY_IN_USE
          ) {
            signInByEmailPassword({ email, password })
              .then((data) => {
                resolve(data);
              })
              .catch((error) => {
                reject(error);
              });
          } else {
            reject(errorCode);
          }
        });
    });

  const signOutUser = () =>
    new Promise((resolve, reject) => {
      const auth = getAuth();
      signOut(auth)
        .then(() => {
          // Sign-out successful.
          signInByAnonymous()
            .then(() => {
              dispatch({
                type: "setProfile",
                payload: null,
              });
              resolve();
            })
            .catch((error) => {
              reject(error);
            });
        })
        .catch((error) => {
          // An error happened.
          reject(error);
        });
    });

  useEffect(() => {
    if (firebase) {
      // Listen authenticated user
      const auth = getAuth();
      if (token.value) {
        console.log("signInWithCustomToken", token.value);
        signInWithCustomToken(auth, token.value)
          .then(async (userCredential) => {
            // Signed in
            const user = userCredential.user;
            await loadUser(user);
          })
          .catch((error) => {
            const errorCode = error.code;
            signInByAnonymous();
            console.log("errorCode", errorCode);
          });
      } else {
        signInByAnonymous();
      }
    }
    return () => {};
  }, [firebase]);

  return (
    <UserContext.Provider
      value={{
        profile: state.profile,
        signInUser,
        signOutUser,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => useContext(UserContext);

export default UserStore;
