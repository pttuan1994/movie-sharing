import { useReducer, createContext, useContext, useEffect } from "react";
import { produce } from "immer";
import { getAuth } from "firebase/auth";
import { getFunctions } from "firebase/functions";
import { getDatabase } from "firebase/database";

import { createFirebaseApp } from "@/firebase/clientApp";

const initialState = {
  firebase: null,
};

const reducer = produce((draft, action) => {
  switch (action.type) {
    case "setFirebase":
      draft.firebase = action.payload;
      break;
    default:
      break;
  }
});

const CommonContext = createContext({});

const CommonStore = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  useEffect(() => {
    //Initial Firebase
    const app = createFirebaseApp();
    getAuth(app);
    getFunctions(app);
    getDatabase(app);
    dispatch({
      type: "setFirebase",
      payload: app,
    });
  }, []);
  return (
    <CommonContext.Provider
      value={{
        firebase: state.firebase,
      }}
    >
      {children}
    </CommonContext.Provider>
  );
};

export const useCommon = () => useContext(CommonContext);

export default CommonStore;
