import { useReducer, createContext, useContext, useEffect } from "react";
import axios from "axios";
import { getAuth } from "firebase/auth";
import {
  getDatabase,
  ref,
  update,
  remove,
  query,
  limitToLast,
  orderByChild,
  onValue,
  endBefore,
  startAfter,
  onChildAdded,
  off,
} from "firebase/database";
import { produce } from "immer";
import { useCommon } from "./common";

const youtubeApi = axios.create({
  baseURL: `https://www.googleapis.com/youtube/v3`,
  headers: {
    "Content-Type": "application/json",
  },
  withCredentials: false,
});

const initialState = {
  loading: false,
  hasMore: true,
  initialTime: Date.now(),
  time: Date.now(),
  list: [],
};

const COUNT = 3;

const reducer = produce((draft, action) => {
  switch (action.type) {
    case "setList": {
      const results = action.payload;
      draft.list = draft.list.concat(results);
      if (results.length) {
        draft.time = results[0].createdAt;
        draft.hasMore = results.length === COUNT;
      } else {
        draft.hasMore = false;
      }
      break;
    }
    case "setLoading": {
      draft.loading = action.payload;
      break;
    }
    case "addVideo": {
      const video = action.payload;
      draft.list = [{ ...video }].concat(draft.list);
      break;
    }
    default:
      break;
  }
});

const VideoContext = createContext({});

const VideoStore = ({ children }) => {
  const { firebase } = useCommon();
  const [state, dispatch] = useReducer(reducer, initialState);

  const getId = (url) => {
    const regex =
      /(youtu.*be.*)\/(watch\?v=|embed\/|v|shorts|)(.*?((?=[&#?])|$))/gm;
    const result = regex.exec(url) || [];
    return result[3];
  };

  const search = async (url) => {
    let result;
    const id = getId(url);
    if (id) {
      try {
        const { data } = await youtubeApi.get(
          `/videos?part=snippet&id=${id}&key=${process.env.NEXT_PUBLIC_FIREBASE_API_KEY}`
        );
        const { snippet } = data.items[0];
        result = {
          id,
          url,
          ...snippet,
        };
      } catch (ex) {
        console.log(ex);
      }
    }
    return result;
  };

  const share = async (video) => {
    try {
      const auth = getAuth();
      const database = getDatabase();
      const { email } = auth.currentUser;
      const videoRef = ref(database, "videos/" + video.id);
      await update(videoRef, {
        email,
        createdAt: Date.now(),
        votes: {},
        ...video,
      });
      return true;
    } catch (ex) {
      console.log(ex);
      return false;
    }
  };

  const unVote = async ({ video }) => {
    try {
      const auth = getAuth();
      const database = getDatabase();
      const { uid } = auth.currentUser;
      const userRef = ref(database, `videos/${video.id}/votes/${uid}`);
      await remove(userRef);
      return true;
    } catch (ex) {
      console.log(ex);
      return false;
    }
  };

  const vote = async ({ video, like }) => {
    try {
      const auth = getAuth();
      const database = getDatabase();
      const { uid } = auth.currentUser;
      const userRef = ref(database, `videos/${video.id}/votes`);
      await update(userRef, { [uid]: like });
      return true;
    } catch (ex) {
      console.log(ex);
      return false;
    }
  };

  const getList = () => {
    dispatch({
      type: "setLoading",
      payload: true,
    });
    const database = getDatabase();
    const dataRef = query(
      ref(database, "videos"),
      endBefore(state.time),
      orderByChild("createdAt"),
      limitToLast(COUNT)
    );
    onValue(
      dataRef,
      (snapshot) => {
        const results = [];
        snapshot.forEach((childSnapshot) => {
          if (childSnapshot) {
            const childData = childSnapshot.val();
            results.push({ ...childData });
          }
        });
        dispatch({
          type: "setList",
          payload: results,
        });
        dispatch({
          type: "setLoading",
          payload: false,
        });
      },
      {
        onlyOnce: true,
      }
    );
  };

  useEffect(() => {
    if (firebase) {
      const database = getDatabase();
      const dataRef = query(
        ref(database, "videos"),
        startAfter(state.initialTime),
        orderByChild("createdAt"),
        limitToLast(1)
      );
      onChildAdded(dataRef, (data) => {
        const result = data.val();
        console.log("video added", result);
        dispatch({
          type: "addVideo",
          payload: result,
        });
      });
      return () => {
        off(dataRef);
      };
    }
  }, [firebase]);

  return (
    <VideoContext.Provider
      value={{
        ...state,
        search,
        share,
        vote,
        unVote,
        getList,
      }}
    >
      {children}
    </VideoContext.Provider>
  );
};

export const useVideo = () => useContext(VideoContext);

export default VideoStore;
