export const ACCESS_TOKEN = "accessToken";
export const ERROR_CODE = {
  INVALID_CUSTOM_TOKEN: "auth/invalid-custom-token",
  USER_NOT_FOUND: "auth/user-not-found",
  PROVIDER_ALREADY_LINKED: "auth/provider-already-linked",
  EMAIL_ALREADY_IN_USE: "auth/email-already-in-use",
};
