import { Layout as LayoutAntd } from "antd";
import useWindowDimensions from "@/utils/useWindowDimensions";
import Header from "./header";
import Footer from "./footer";

const Layout = ({ children }) => {
  const { device } = useWindowDimensions();
  return (
    <>
      <Header></Header>
      <LayoutAntd
        style={{
          minHeight: "calc(100vh - 134px)",
          paddingTop: device === "desktop" ? "1rem" : "11rem",
        }}
      >
        <div className="container">{children}</div>
      </LayoutAntd>
      <Footer></Footer>
    </>
  );
};

export default Layout;
