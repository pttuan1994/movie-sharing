import { useState } from "react";
import { Button, message } from "antd";
import styled from "styled-components";
import { useRouter } from "next/router";
import useWindowDimensions from "@/utils/useWindowDimensions";
import { useUser } from "@/store/user";

const MenuUser = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  & > * {
    margin: 2px 5px;
  }
  p {
    ${({ device }) =>
      device === "desktop"
        ? `display: block;`
        : `display: none;
    `}
    line-height: 20px;
  }
  ${({ device }) =>
    device === "desktop"
      ? `position: initial;`
      : `position: absolute;
        top: 6rem;
        color: #000;
    `}
`;

const Menu = () => {
  const { device } = useWindowDimensions();
  const router = useRouter();
  const { profile, signOutUser } = useUser();
  const [loading, setLoading] = useState(false);
  const onSignOut = async () => {
    setLoading(true);
    try {
      await signOutUser();
    } catch (error) {
      message.error(error);
    }
    setLoading(false);
  };
  return (
    <MenuUser device={device}>
      <p>Welcome {profile.email}</p>
      <Button
        type="primary"
        onClick={() => {
          router.push("/share", undefined, { shallow: true });
        }}
      >
        Share a movie
      </Button>
      <Button loading={loading} type="secondary" onClick={onSignOut}>
        Logout
      </Button>
    </MenuUser>
  );
};

export default Menu;
