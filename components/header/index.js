import { Layout } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import styled from "styled-components";
import { useRouter } from "next/router";
import { useUser } from "@/store/user";
import Welcome from "./Welcome";
import Menu from "./Menu";

const { Header: HeaderAntd } = Layout;

const Header = styled(HeaderAntd)`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Logo = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  color: #fff;
  font-size: 1.5rem;
  p {
    margin: 0 1rem;
  }
`;

const HeaderWrapper = () => {
  const router = useRouter();
  const { profile } = useUser();
  return (
    <Header>
      <Logo
        onClick={() => {
          router.push("/", undefined, { shallow: true });
        }}
      >
        <HomeOutlined />
        <p role="heading">Funny Movies</p>
      </Logo>
      {profile?.email ? <Menu></Menu> : <Welcome></Welcome>}
    </Header>
  );
};

export default HeaderWrapper;
