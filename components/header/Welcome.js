import { useState } from "react";
import { Form, Input, Button, message } from "antd";
import styled from "styled-components";
import useWindowDimensions from "@/utils/useWindowDimensions";
import { useUser } from "@/store/user";

const WelcomeForm = styled(Form)`
  ${({ device }) =>
    device === "desktop"
      ? `position: initial;`
      : `position: absolute;
        top: 6rem;
        justify-content: center;`}
  & .ant-form-item {
    margin-top: 5px;
    margin-bottom: 5px;
  }
`;

const Welcome = () => {
  const { device } = useWindowDimensions();
  const [form] = Form.useForm();
  const { signInUser } = useUser();
  const [loading, setLoading] = useState(false);
  const onFinish = async (values) => {
    setLoading(true);
    try {
      await signInUser(values);
    } catch (error) {
      message.error(error);
    }
    setLoading(false);
  };
  const onFinishFailed = ({ errorFields }) => {
    const [first] = errorFields;
    const [error] = first.errors;
    message.error(error);
  };

  return (
    <>
      <WelcomeForm
        device={device}
        layout={"inline"}
        form={form}
        initialValues={{
          email: "",
          password: "",
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item name="email" rules={[{ required: true }]} help={false}>
          <Input type="email" placeholder="Email" />
        </Form.Item>
        <Form.Item name="password" rules={[{ required: true }]} help={false}>
          <Input.Password placeholder="Password" />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            loading={loading}
            role="submit"
          >
            Login / Register
          </Button>
        </Form.Item>
      </WelcomeForm>
    </>
  );
};

export default Welcome;
