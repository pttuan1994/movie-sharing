import { Layout } from "antd";
const { Footer: FooterAntd } = Layout;

const Footer = () => {
  return (
    <FooterAntd style={{ textAlign: "center" }} role="heading">
      Movie Sharing ©{new Date().getFullYear()} Created by Tuan Phan
    </FooterAntd>
  );
};

export default Footer;
