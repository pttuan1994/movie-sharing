import { List as ListAntd, message } from "antd";
import styled from "styled-components";
import {
  LikeOutlined,
  DislikeOutlined,
  LikeFilled,
  DislikeFilled,
} from "@ant-design/icons";
import { createElement, useEffect, useState } from "react";
import { getDatabase, ref, onValue, off } from "firebase/database";
import { useVideo } from "@/store/video";
import { useUser } from "@/store/user";

const IconText = ({ icon, text, onClick }) => (
  <button
    className="flex items-center justify-start mr-4 text-xl"
    onClick={onClick}
  >
    {createElement(icon)}
    {text}
  </button>
);

const Video = styled.div`
  position: relative;
  padding-bottom: 56.25%; /* 16:9 */
  width: 100%;
  height: 0;
  iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
`;

const Item = ({ item }) => {
  const { profile } = useUser();
  const { vote, unVote } = useVideo();
  const [entries, setEntries] = useState(Object.entries(item.votes || {}));
  const [uid, like] = entries.find(([id]) => id && id === profile?.uid) || [];
  const likeCount = entries.reduce((value, [uid, like]) => {
    return like ? value + 1 : value;
  }, 0);
  const unlikeCount = entries.length - likeCount;
  useEffect(() => {
    if (item.id) {
      const database = getDatabase();
      const dataRef = ref(database, `videos/${item.id}/votes`);
      onValue(
        dataRef,
        (snapshot) => {
          const result = snapshot.val();
          setEntries(Object.entries(result || {}));
        },
        {
          onlyOnce: false,
        }
      );
      return () => {
        off(dataRef);
      };
    }
  }, [item]);
  return (
    <ListAntd.Item>
      <div className="grid grid-cols-2 gap-y-8 gap-x-16">
        <div className="col-span-2 lg:col-span-1 flex justify-start items-start">
          <Video>
            <iframe
              width="560"
              height="315"
              src={`https://www.youtube.com/embed/${item.id}`}
              frameBorder="0"
              allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            />
          </Video>
        </div>
        <div className="col-span-2 lg:col-span-1 flex flex-col justify-center items-start space-y-2">
          <h2 className="text-xl">
            <a target={"_blank"} href={item.url}>
              {item.title}
            </a>
          </h2>
          <p className="text-sm">Shared by: {item.email}</p>
          <div className="flex items-center justify-start">
            <IconText
              icon={like === true ? LikeFilled : LikeOutlined}
              text={likeCount}
              key="list-vertical-like-o"
              onClick={() => {
                if (!profile) {
                  message.info("You need login/register to vote video");
                  return;
                }
                if (like === true) {
                  unVote({ video: item });
                } else {
                  vote({ video: item, like: true });
                }
              }}
            />
            <IconText
              icon={like === false ? DislikeFilled : DislikeOutlined}
              text={unlikeCount}
              key="list-vertical-star-o"
              onClick={() => {
                if (!profile) {
                  message.info("You need login/register to vote video");
                  return;
                }
                if (like === false) {
                  unVote({ video: item });
                } else {
                  vote({ video: item, like: false });
                }
              }}
            />
          </div>
          <div className="text-sm">
            <p className="m-0">Description:</p>
            <p dangerouslySetInnerHTML={{ __html: item.description }}></p>
          </div>
        </div>
      </div>
    </ListAntd.Item>
  );
};

export default Item;
