import Layout from "@/components/Layout";
import List from "./List";

const Home = () => {
  return (
    <Layout>
      <List></List>
    </Layout>
  );
};

export default Home;
