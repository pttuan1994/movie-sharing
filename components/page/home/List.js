import { List as ListAntd, Skeleton, Divider } from "antd";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import styled from "styled-components";
import { useVideo } from "@/store/video";
import Item from "./Item";
import { useCommon } from "@/store/common";

const ScrollWrapper = styled.div`
  height: 80vh;
  overflow: auto;
  padding: 0 16px;
`;

const List = () => {
  const { firebase } = useCommon();
  const { loading, hasMore, list, getList } = useVideo();

  useEffect(() => {
    if (firebase) {
      loadMoreData();
    }
  }, [firebase]);

  const loadMoreData = () => {
    if (!loading) {
      getList();
    }
    // Resetting window's offsetTop so as to display react-virtualized demo underfloor.
    // In real scene, you can using public method of react-virtualized:
    // https://stackoverflow.com/questions/46700726/how-to-use-public-method-updateposition-of-react-virtualized
    window.dispatchEvent(new Event("resize"));
  };

  return (
    <ScrollWrapper id="scrollableDiv">
      <InfiniteScroll
        dataLength={list.length}
        next={loadMoreData}
        hasMore={hasMore}
        loader={<Skeleton avatar={false} paragraph={{ rows: 3 }} active />}
        endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
        scrollableTarget="scrollableDiv"
      >
        <ListAntd
          dataSource={list}
          renderItem={(item) => <Item item={item}></Item>}
        />
      </InfiniteScroll>
    </ScrollWrapper>
  );
};

export default List;
