import Layout from "@/components/Layout";
import { Button, Form, Input, message } from "antd";
import styled from "styled-components";
import useWindowDimensions from "@/utils/useWindowDimensions";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useUser } from "@/store/user";
import { useVideo } from "@/store/video";

const Wrapper = styled.div`
  position: absolute;
  width: 100%;
  max-width: 500px;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  padding: 40px 20px;
  border: 1px solid rgba(140, 140, 140, 0.35);
`;

const Label = styled.h2`
  position: absolute;
  font-size: 1rem;
  top: -16px;
  left: 20px;
  padding: 0 5px;
  background: rgb(240, 242, 245);
`;

const Share = () => {
  const { device } = useWindowDimensions();
  const router = useRouter();
  const { profile } = useUser();
  const { search, share } = useVideo();
  const [searchLoading, setSearchLoading] = useState(false);
  const [shareLoading, setShareLoading] = useState(false);
  const [video, setVideo] = useState({});

  const onShare = async () => {
    setShareLoading(true);
    const result = await share(video);
    if (result) {
      setVideo({});
      message.success("Video is shared successfully");
    } else {
      message.error("Cannot share video");
    }
    setShareLoading(false);
  };

  const onSearch = async (value) => {
    setSearchLoading(true);
    const result = await search(value);
    if (result) {
      setVideo(result);
    } else {
      setVideo({});
      message.error("Cannot found video");
    }
    setSearchLoading(false);
  };

  useEffect(() => {
    if (!profile) {
      router.push("/", undefined, { shallow: true });
    }
  }, [profile]);

  return (
    <Layout>
      <Wrapper>
        <Label>Share a Youtube movie</Label>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{}}
          autoComplete="off"
        >
          <Form.Item
            label="Youtube URL:"
            name="url"
            rules={[{ required: true, message: "Please input Youtube URL!" }]}
          >
            <Input.Search
              placeholder="input Youtube URL"
              allowClear
              enterButton="Search"
              size="large"
              onSearch={onSearch}
              loading={searchLoading}
            />
          </Form.Item>
          {video.thumbnails ? (
            <Form.Item
              wrapperCol={device === "desktop" ? { offset: 8, span: 16 } : {}}
            >
              <div>
                <img
                  width="100%"
                  height="100%"
                  alt="thumbnails"
                  src={video.thumbnails.high?.url}
                ></img>
              </div>
              <Button
                style={{ width: "100%" }}
                type="primary"
                htmlType="button"
                loading={shareLoading}
                onClick={onShare}
              >
                Share
              </Button>
            </Form.Item>
          ) : null}
        </Form>
      </Wrapper>
    </Layout>
  );
};

export default Share;
