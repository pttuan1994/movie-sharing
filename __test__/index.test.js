import "./__mock__/app.mock";
import "whatwg-fetch";
import { render, screen } from "@testing-library/react";
import Header from "@/components/header";
import Welcome from "@/components/header/Welcome";
import Footer from "@/components/footer";

describe("Header", () => {
  it("renders a heading header", () => {
    render(<Header />);

    const heading = screen.getByRole("heading", {
      name: /Funny Movies/i,
    });

    expect(heading).toBeInTheDocument();
  });
  it("renders a button login/register", async () => {
    render(<Welcome />);

    const button = screen.getByText(/Login \/ Register/i);

    expect(button).toBeInTheDocument();
  });
});

describe("Footer", () => {
  it("renders a heading footer", () => {
    render(<Footer />);

    const heading = screen.getByRole("heading", {
      name: `Movie Sharing ©${new Date().getFullYear()} Created by Tuan Phan`,
    });

    expect(heading).toBeInTheDocument();
  });
});
