This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run build packages:

```bash
npm install
# or
yarn install
```

Second, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Then, run the build to testing and deploy production server:

```bash
npm run build
# or
yarn build
```

Run unit test:

```bash
npm run test
# or
yarn test
```

Run integration test:

```bash
npm run e2e
# or
yarn e2e
```

## Learn More

You can check out [Movie Sharing repository](https://gitlab.com/pttuan1994/movie-sharing.git) - your feedback and contributions are welcome!

## Deploy on Vercel

Visit my [Movie Sharing website](https://movie-sharing.vercel.app/) for more details.
