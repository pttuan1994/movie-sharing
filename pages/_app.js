import ThemeProvider from "@/styles/styledGlobal";
import StoreProvider from "@/store/index";
import "styles/tailwindGlobal.css";
import "antd/dist/antd.css";
const App = ({ Component, pageProps }) => {
  return (
    <StoreProvider>
      <ThemeProvider>
        <Component {...pageProps} />
      </ThemeProvider>
    </StoreProvider>
  );
};

export default App;
