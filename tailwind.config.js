module.exports = {
  important: true,
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [
    ({ addComponents, theme }) => {
      addComponents({
        ".container": {
          "@apply mx-auto": {},
          "@apply px-8": {},
        },
      });
    },
  ],
};
